//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    // wx.login({
    //   success: res => {
    //     //session 未过期，并且在本生命周期一直有效
    //     console.log('登陆态未过期，直接登陆:',res);
    //     // 发送 res.code 到后台换取 openId, sessionKey, unionId
    //     wx.request({
    //       url: 'https://test.com/onLogin',
    //       data: {
    //         code: res.code
    //       }
    //     })
    //   },
    //   fail:function(){
    //     wx.login({
    //       success: res => {
    //         // 发送 res.code 到后台换取 openId, sessionKey, unionId
    //         console.log('登陆态过期，重新登陆:',res);
    //       }
    //     })
    //   }
    // })
    wx.checkSession({
      success:()=>{
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      },
      fail:()=>{
        //重新登陆
        wx.login({
          success: res => {
            console.log('登陆态过期，重新登陆:',res);
          }
        })
      }
    })

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            withCredentials:true,
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
              console.log('getUserInfo',res)
            }
          })
        }
        
      },
      fail:res => {
        console.log('fail',res);
      }
    })
  },
  globalData: {
    userInfo: null,
    gameGridNum:9,
  }
})