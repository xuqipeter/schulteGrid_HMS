// pages/login/login.js
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    date: "2000-01-01",
    radioItems: [{
        name: '男',
        value: 'boy'
      },
      {
        name: '女',
        value: 'girl',
        checked: true
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /* 选择时间 */
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  /* 选择性别 */
  radioChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value);

    var radioItems = this.data.radioItems;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }
    this.setData({
      radioItems: radioItems
    });
  },
  /* 注册新账号 */
  signUp: function(e){
    console.log('form发生了submit事件，携带数据为：', e.detail.value)
    var obj_val=e.detail.value;
    var arr_key =Object.keys(obj_val);
    var len=arr_key.length;
    /* var toastText={
      userName:'请填写昵称',
    }
    for(var i= 0; i < len; i++) {
      var key=arr_key[i];
      if(obj_val[key]===''){
        console.log(key);
        wx.showToast({
          title:toastText[key]
        })
        return false;
      }
    } */
    /* wx.request({
      url:'',
      data:obj_val,
      method:POST,
      dataType:json,
      success:(data)=>{

      }
    }) */
    var age=3;
    if(age<3){
      app.globalData.gameGridNum=9;
    }else if(3<=age<4.5){
      app.globalData.gameGridNum=16;
    }else{
      app.globalData.gameGridNum=25;
    }
    wx.redirectTo({
      url:'../game/game'
    })
  }
})