//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false, //初始状态没有userInfo数据
    canIUse: wx.canIUse('button.open-type.getUserInfo'), //判断微信是否能用这个api
    setUserInfo:false,
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
      console.log('1:', app.globalData.userInfo);
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
      console.log('3:', app.globalData.userInfo);
    }
    // this.login();
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  login: function () {
    var $this = this
    if (typeof success == "function") {
      this.data.getUserInfoSuccess = success
    }
    wx.login({
      success: function (res) {
        var code = res.code;
        wx.getUserInfo({
          success: function (res) {
            // 平台登陆
            $this.setData({
              userInfo: res.userInfo,
              hasUserInfo: true
            })
            console.log('success',$this.data.hasUserInfo,res);
            wx.redirectTo({
              url:'../login/login'
            })
          },
          fail: function (res) {
            $this.setData({
              setUserInfo: true
            })
            // $this.openSetting();
            console.log('fail',$this.data.hasUserInfo,res);
          }
        })
      }
    })
  },
  //跳转设置页面授权
  openSetting: function () {
    var $this = this
    if (wx.openSetting) {
      wx.openSetting({
        success: function (res) {
          //尝试再次登录
          $this.login()
        }
      })
    } else {
      wx.showModal({
        title: '授权提示',
        content: '小程序需要您的微信授权才能使用哦~ 错过授权页面的处理方法：删除小程序->重新搜索进入->点击授权按钮'
      })
    }
  }

})